package fi.vamk.e1801917.ass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class AttendanceController {
    @Autowired
    private AttendanceRepository repository;

    @GetMapping("/attendances")
    public Iterable<Attendance_Input> list() {
    	ArrayList< Attendance_Input> list= new ArrayList<Attendance_Input>();
    	for (Attendance a : repository.findAll()) {
			list.add(a.convert());
		}
        return list;
    }
    
    
    
    @GetMapping("/attendances/{date}")
    public Attendance_Input getByDate(@PathVariable("date") String date) {
		try {			
			Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(date);
			return repository.findByDay(date1).convert();
		} catch (ParseException e) {
			return null;
		}
		
    }

    @PostMapping("/attendance")
    public @ResponseBody Attendance_Input create(@RequestBody Attendance_Input item) {
    	Attendance a= item.convert();
    	if (a.getDay() == null) return null;
        return repository.save(a).convert();
    }

    @PutMapping("/attendance")
    public @ResponseBody Attendance_Input update(@RequestBody Attendance_Input item) {
    	Attendance a= item.convert();
    	if (a.getDay() == null) return null;
        return repository.save(a).convert();
    }

    @DeleteMapping("/attendance")
    public void date(@RequestBody Attendance_Input item) {
    	Attendance a= item.convert();
    	repository.delete(a);
    }
}