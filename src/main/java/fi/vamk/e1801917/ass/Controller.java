package fi.vamk.e1801917.ass;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class Controller {
    @RequestMapping("/test")
    public String test() {
        return "{\"id\":1}";
    }
}